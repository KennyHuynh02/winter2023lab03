import java.util.Scanner;
class ApplianceStore {
	public static void main(String[]args){
		
		Scanner keyboard = new Scanner(System.in);
		Toaster[] toasters = new Toaster[4];
		for(int i=0; i<toasters.length; i++){
			toasters[i] = new Toaster();
		}
		
		for(int j=0, k=1; j<toasters.length; j++, k++){
			System.out.println("what is the color of your " + k + " toaster?");
			toasters[j].color = keyboard.next();
			System.out.println("what is the max Watt of your " + k + " toaster?");
			toasters[j].maxWatt = keyboard.nextInt();
			System.out.println("what is the price of your " + k + " toaster?");
			toasters[j].price = keyboard.nextInt();
		}
		
		System.out.println("The color of your last appliance is " + toasters[toasters.length-1].color);
		System.out.println("The maxWatt of your last appliance is " + toasters[toasters.length-1].maxWatt);
		System.out.println("The price of your last appliance is " + toasters[toasters.length-1].price);
		
		toasters[0].wattRange();
		toasters[0].priceRange();
		
	} 
}