class Toaster {
	public String color;
	public int maxWatt;
	public int price;

	public void wattRange(){
		if(this.maxWatt < 800){
			System.out.println("Your toaster's max Watt is low");
		}
		else if (this.maxWatt >= 800 && this.maxWatt <= 1500){
			System.out.println("Your toaster's max Watt is average");
		}
		else {
			System.out.println("your toaster's max Watt is above average!");
		}
	}

	public void priceRange(){
		if(this.price < 20){
			System.out.println("Your toaster's price is lower than average");
		}
		else if (this.price >= 20 && this.price <= 40){
			System.out.println("Your toaster's price is average");
		}
		else {
			System.out.println("your toaster's price is abore average!");
		}
	}
}